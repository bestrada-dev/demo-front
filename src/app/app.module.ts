import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpModule } from '@angular/http';
import {routing} from './app.routing';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { Menu } from './components/menu/menu.component';
import { homeComponent } from './components/home.component';
import { parametrosComponent } from './components/parametros/parametros.component';

import {ParametrosService} from './services/parametros/parametros.service';

@NgModule({
  declarations: [
  AppComponent,
  Menu,
  homeComponent,
  parametrosComponent
  ],
  imports: [
  BrowserModule,
  FormsModule,
  HttpModule,
  routing,
  DataTablesModule,
  ReactiveFormsModule
  ],
  providers: [
  ParametrosService
  ],
  bootstrap: [
  AppComponent, 
  Menu
  ]
})
export class AppModule { }
