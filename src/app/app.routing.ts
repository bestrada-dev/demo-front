import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Menu }  from './components/menu/menu.component';
import { homeComponent }  from './components/home.component';
import { parametrosComponent }  from './components/parametros/parametros.component';


const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/parametros',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: homeComponent
  },
  {
    path: 'parametros',
    component: parametrosComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);