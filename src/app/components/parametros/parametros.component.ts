import {Component, ElementRef, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';

import {ParametrosService} from '../../services/parametros/parametros.service';
import {Parametro} from '../../models/parametros/parametro';


@Component({
	selector: 'parametros',
	templateUrl: './parametros.component.html'
})
export class parametrosComponent implements OnInit{
	title = "Lista de parámetros. Texto prueba";

	rootNode :  any;
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();

	parametros: Parametro[];
	

	constructor (private parametroService: ParametrosService, private router: Router, rootNode: ElementRef) {

		this.parametroService.getParametros().subscribe(
			data => {console.log(this.parametros = JSON.parse(JSON.parse(JSON.stringify(data))._body))
				this.dtTrigger.next();},
				error => console.log(error)


				);
		this.rootNode =  rootNode;

	}

	ngOnInit(): void {
		this.dtOptions = {
			pagingType: 'full_numbers',
			pageLength: 10
		};

	}



	
}