import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';

import {Parametro} from '../../models/parametros/parametro';

@Injectable()
export class ParametrosService {

  constructor (private http:Http){}

  getParametros() {
    //let url = "http://localhost:8099/api/ParParametro/all";
    let url = "http://localhost:8088/demo-back/api/ParParametro/all";
    return this.http.get(url);
  }

  

}
