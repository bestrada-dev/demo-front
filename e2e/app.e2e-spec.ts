import { InvFrontPage } from './app.po';

describe('inv-front App', function() {
  let page: InvFrontPage;

  beforeEach(() => {
    page = new InvFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works! <a href="http://localhost:4200/api/productos">Productos<a>');
  });
});
